# get data from db; save result as csv; store csv in web directory; archivate all csv files

#!/bin/bash
sleep 5

echo "ZipcodeLookup.csv ..."

PGPASSWORD=[removed db password] /usr/bin/psql --username=user1 --host host1 --port 5432 --dbname db1 -c "\copy ( select t.*, Now() as \"DateTimeFetched\" from dbo.\"ZipcodeLookupReport\" t ) To '/var/www/html/site1/reports/ZipcodeLookup.csv' With CSV header"

echo "removing previous tmp_csv.zip if exists ..."

if [ -f /var/www/html/site1/reports/tmp_csv.zip ]; then
        echo "file /var/www/html/site1/reports/tmp_csv.zip exists, deleting..."
        rm /var/www/html/site1/reports/tmp_csv.zip
        echo ""
else
        echo "file /var/www/html/site1/reports/tmp_csv.zip does not exist"
        echo ""
fi

echo "adding .csv files into tmp_csv.zip ..."

zip -j /var/www/html/site1/reports/tmp_csv.zip /var/www/html/site1/reports/*.csv

echo "reports_csv.zip ..."

cp /var/www/html/site1/reports/tmp_csv.zip /var/www/html/site1/reports/reports_csv.zip

echo "starting data validation..."

sleep 5

/usr/bin/psql -c 'delete from public."ZipcodeLookup"'

/usr/bin/psql -c "COPY public.\"ZipcodeLookup\" (\"Id\",\"IPAddress\",\"Zipcode\",\"SourceCodeId\",\"CreatedDate\",\"UpdatedDate\",\"CreatedBy\",\"UpdatedBy\",\"DateTimeFetched\") FROM '/var/www/html/site1/reports/ZipcodeLookup.csv' DELIMITER ',' CSV HEADER;"

#####
#####
#####

# validate data; send alert if not valid

#!/bin/bash
sleep 239

varZipcodeLookup=(`/usr/bin/psql --username=postgres --dbname postgres -c 'select alert1 from public."vZipcodeLookup"'`)

if [ ${varZipcodeLookup[2]} -gt 0 ]
then
    /usr/sbin/sendmail -vt < /var/lib/pgsql92/ZipcodeLookupAlert.txt
fi