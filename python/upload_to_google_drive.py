#!/usr/bin/env python3
import sys
file = sys.argv[1]
#with open(inFile,'r') as i:
#    lines = i.readlines()

#file = manipulateData(lines)

def main(file):
    from pydrive.auth import GoogleAuth
    from pydrive.drive import GoogleDrive

    settings_path = 'settings.yaml'
    gauth = GoogleAuth(settings_file=settings_path)

    gauth.LoadCredentialsFile("mycreds.txt")
    if gauth.credentials is None:
        gauth.LocalWebserverAuth()
    elif gauth.access_token_expired:
        gauth.Refresh()
    else:
        gauth.Authorize()
    gauth.SaveCredentialsFile("mycreds.txt")

    drive = GoogleDrive(gauth)

    file_list = drive.ListFile({'q': "'folder_id' in parents and trashed=False"}).GetList()
#    print(file_list)
    try:
        for file1 in file_list:
            if file1['title'] == file:
                file1.Delete()                
    except:
        pass
    f = drive.CreateFile({'parents': [{'id': 'folder_id'}]})
    f.SetContentFile(file)
    f.Upload()

if __name__=='__main__':
    main(file)
