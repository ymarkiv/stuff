#!/usr/bin/env python3
import psycopg2 as pg
import pandas.io.sql as psql
import snowflake.connector
import os


print("\n")

import datetime
now = datetime.datetime.now()
print ("Current date and time : ")
print (now.strftime("%Y-%m-%d %H:%M:%S"))

print("\n")

tables = [["sandbox_prod.\"table1\"","TG_APP_DB.TG_STEADY_PROD.SANDBOX_PROD_TABLE1","id","value1","updatedAt"],["public.\"Buildings\"","TG_APP_DB.TG_STEADY_PROD.PUBLIC_BUILDINGS","id","enableSdr1Plus1Autoinvite","updatedAt"]]


for x in tables:
  print("=============================================================\n")
  print("\n")
  print("\n")
  print("table settings:\n")
  print(x)
  print("\n")
  print("source table:\n")
  print(x[0])
  print("\n")
  print("target table:\n")
  print(x[1])
  print("\n")
  print("primary key column:\n")
  print(x[2])
  print("\n")
  print("value to check:\n")
  print(x[3])
  print("\n")
  print("timestamp column:\n")
  print(x[4])
  print("\n")
  print("\n")

  postgres_query = 'Select \"' + x[2] + '\", \"' + x[3] + '\" From ' + x[0]
  print("postgres_query: \n")
  print(postgres_query)

  try: 
     postgres_connection = pg.connect("host=ip1 dbname=db1 user=user1")
     postgres_dataframe = psql.read_sql(postgres_query, postgres_connection)
#     cursor = postgres_connection.cursor()
     print("postgres_dataframe: \n")
     print(postgres_dataframe)

     print("\n")


     try:
        snowflake_connection = snowflake.connector.connect(
            user=os.getenv('SNOWFLAKEUSR'),
            password=os.getenv('SNOWFLAKEPWD'),
            account=os.getenv('SNOWFLAKEACC'),
            role="TG_ANALYST_ROLE",
            warehouse="TG_USER_WH",
            database="TEST_SANDBOX",
            schema="DW_SANDBOX"
            )

#        cur = snowflake_connection.cursor()

#        var_snowflake_sql = "Select " + var_column_target + " From " + var_target
#        print("\n")
#        print("snowflake sql request: ")
#        print(var_snowflake_sql)
#        print("\n")
#        cur.execute("Select " + x[3] + " From " + x[1])

        snowflake_query = 'Select ' + x[2].upper() + ', ' + x[3].upper() + ' From ' + x[1]
        print("snowflake query: \n")
        print(snowflake_query)
        print("\n")
        snowflake_dataframe = psql.read_sql(snowflake_query, snowflake_connection)
        postgres_dataframe.columns = ['a', 'b']
        snowflake_dataframe.columns = ['a', 'b']
#        snowflake_dataframe_rename = snowflake_dataframe.rename(columns={x[2]: x[2].lower()})

        print("snowflake_dataframe: \n")
        print(snowflake_dataframe)
        print("\n")

#        joined = postgres_dataframe.join(snowflake_dataframe, on=x[2])
#        merged = psql.merge(postgres_dataframe, snowflake_dataframe, left_on=x[2], right_on=x[2].upper())
        joined = postgres_dataframe.merge(snowflake_dataframe, on='a')
        print("joined: \n")
        print(joined)
        print("\n")

        joined_diff = joined[joined["b_x"] != joined["b_y"]]
        print("joined_diff: \n")
        print(joined_diff)
        print("\n")

        joined_diff_none = joined_diff[~(joined_diff['b_x'].isnull() & joined_diff['b_y'].isnull())]
        print("joined_diff_none: \n")
        print(joined_diff_none)
        print("\n")

        joined_diff_none_id = joined_diff_none['a']
        print("joined_diff_none_id: \n")
        print(joined_diff_none_id)
        print("\n")

        joined_diff_none_id_list = joined_diff_none_id.values.tolist()
        print("joined_diff_none_id_list: \n")
        print(joined_diff_none_id_list)
        print("\n")

        ids_to_update = ','.join([str(i) for i in joined_diff_none_id_list])
        print("ids_to_update: \n")
        print(ids_to_update)
        print("\n")

        cursor = postgres_connection.cursor()

        if len(joined_diff_none_id_list)>0:
            update_query = "Update " + x[0] + " Set \"" + x[4] + "\" = Now() Where \"" + x[2] + "\" in (" + ids_to_update + ")"
            print("update_query: \n")
            print(update_query)
            print("\n")
###            cursor = postgres_connection.cursor()
            print("running postgres update query... \n")
            cursor.execute(update_query)

     except Exception as error:
         print ("Error while fetching data from Snowflake", error)

     finally:
         #closing database connection.
         if(snowflake_connection):
#             snowflake_connection.commit()
#             cur.close()
             snowflake_connection.close()
             print("Snowflake connection is closed")

     print("\n")
     print("Snowflake end \n")
     print("\n")



  except (Exception, pg.Error) as error :
      print ("Error while fetching data from PostgreSQL", error)

  finally:
      if(postgres_connection):
          postgres_connection.commit()
          cursor.close()
          postgres_connection.close()
          print("PostgreSQL connection is closed \n")

#  print("Postgres end \n")
