#!/usr/bin/env python

import psycopg2

postgres_array = []
snowflake_array = []

print("Postgres start \n")

try:
   connection = psycopg2.connect(user="postgres_username",
                                  password="postgres_password",
                                  host="127.0.0.1",
                                  port="35432",
                                  database="theguarantors_staging")
   cursor = connection.cursor()
   postgreSQL_select_Query = "select * from sandbox.test_deleted"

   cursor.execute(postgreSQL_select_Query)
   print("Selecting rows from Postgres Monolith staging table test_deleted using cursor.fetchall")
   test_deleted_records = cursor.fetchall() 
   
   print("Print each row and its columns values")
   for row in test_deleted_records:
       print(row[0])
       postgres_array.append(row[0])

except (Exception, psycopg2.Error) as error :
    print ("Error while fetching data from PostgreSQL", error)

finally:
    #closing database connection.
    if(connection):
        cursor.close()
        connection.close()
        print("PostgreSQL connection is closed")

print("Postgres end \n")

print("\n")
print("\n")
print("\n")

print("Snowflake start \n")

import snowflake.connector

conn = snowflake.connector.connect(
    user="snowflake_user",
    password="snowflake_password",
    account="vsa09544.us-east-1",
    warehouse="TG_USER_WH",
    database="TEST_SANDBOX",
    schema="DW_SANDBOX"
    )

cur = conn.cursor()
try:
    cur.execute("SELECT id FROM TEST_SANDBOX.DW_SANDBOX.test_deleted ORDER BY id")
    for row in cur:
        print(row[0])
        snowflake_array.append(row[0])

    print("\n")
    print("\n")
    print("\n")

    print("postgres array: \n")
    print(postgres_array)
    print("\n")
    print("\n")
    print("\n")
    print("snowflake array: \n")
    print(snowflake_array)
    print("\n")
    print("\n")

    print("compare arrays postgres vs snowflake: \n")
    for i in snowflake_array:
        if i not in postgres_array:
            print(i)
            print("delete ID from Snowflake table:" + str(i) + "\n")
            conn.cursor().execute(
                "DELETE FROM TEST_SANDBOX.DW_SANDBOX.test_deleted WHERE ID = %s",(i))

    print("\n")
    print("\n")
    print("\n")

finally:
    cur.close()

print("\n")
print("Snowflake end \n")
print("\n")