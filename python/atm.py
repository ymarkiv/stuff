#ATM - return required sum using minimum amount of 100, 50 and 20 banknotes.
#Just to simplify, the input is always positive; also the input is always divisible using mentioned banknotes nominals.
def atm(amount):

      Opt = [0 for i in range(0, amount+1)]
      sets = {i:[] for i in range(amount+1)}
      banknotes = [20, 50, 100]
      n = len(banknotes)
      for i in range(1, amount+1):
            smallest = float("inf")
            for j in range(0, n):
                 if (banknotes[j] <= i):
                       smallest = min(smallest, Opt[i - banknotes[j]]) 
                       if smallest == Opt[i - banknotes[j]]:
                             sets[i] = [banknotes[j]] + sets[i-banknotes[j]]
            Opt[i] = 1 + smallest 
      banknotes = sorted(sets[amount])
      var100 = 0
      var50 = 0 
      var20 = 0
      for x in banknotes: 
          if x == 100:
              var100 = var100 + 1
          elif x == 50: 
              var50 = var50 + 1
          else: 
              var20 = var20 + 1
      result = str(var100) + ' ' + str(var50) + ' ' + str(var20)
      return result
	
print (str(110) + ': ' + atm(110))
print (str(120) + ': ' + atm(120))
print (str(40) + ': ' + atm(40))
print (str(90) + ': ' + atm(90))
print (str(190) + ': ' + atm(190))
print (str(210) + ': ' + atm(210))
print (str(10) + ': ' + atm(10))


# 120: 1 0 1                                               
# 40: 0 0 2                                                
# 90: 0 1 2                                                
# 190: 1 1 2                                                
# 210: 1 1 3                                                
# 10: 0 0 0 #all zeros mean that input value is not divisible using available banknotes nominals