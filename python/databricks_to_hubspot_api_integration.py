min_likelihood_to_close = 0
max_likelihood_to_close = 100

# File location and type
#file_location = "/FileStore/tables/databricks_deal_score___Sheet1.csv"
#file_location = "/FileStore/tables/databricks_deal_score___Sheet1__1_.csv"
#file_location = "/FileStore/tables/databricks_deal_score___Sheet1__2_.csv"
#file_location = "/FileStore/tables/databricks_deal_score___Sheet1__3_.csv"
#file_location = "/FileStore/tables/databricks_deal_score___Sheet1__4_.csv"
file_location = "/FileStore/tables/databricks_deal_score___Sheet1__5_.csv"
file_type = "csv"

def update_likelihood_to_close(par_hubspot_deal_id, par_likelihood_to_close):
  import json
  # https://pypi.org/project/hubspot-api-client/
  import requests

  url = "https://api.hubapi.com/crm/v3/objects/deals/" + par_hubspot_deal_id

  #key_file_location = "/FileStore/tables/databricks_deal_score___Sheet1__2_.csv"
  key_file_location = "/FileStore/tables/hubspot_key___Sheet1.csv"
  #key_file_location = "/FileStore/tables/hubspot_key___Sheet1__1_.csv"
  infer_schema = "false"
  first_row_is_header = "false"
  delimiter = ","
  df = spark.read.format(file_type) \
    .option("inferSchema", infer_schema) \
    .option("header", first_row_is_header) \
    .option("sep", delimiter) \
    .load(key_file_location)

  var_key = ""
  for row in df.rdd.collect():
      #print("current table record: \n")
      #print(row)
      #print("\n")
      #print("current _c0 (hubspot key) column: \n")
      #print(row._c0)
      var_key = row._c0
      print("\n")
  
  querystring = json.loads('{"hapikey":"' + var_key + '"}')
  
  #print("auth_query_string:")
  #print(querystring)
  print("\n")

  payload = "{\"properties\":{\"likelihood_to_close\":\"" + par_likelihood_to_close + "\"}}"
  headers = {
      'accept': "application/json",
      'content-type': "application/json"
      }

  response = requests.request("PATCH", url, data=payload, headers=headers, params=querystring)

  print("\n")
  #resp = str(response)
  #print(response.text)
  resp = response.text
  
  try:
    #print(resp)
    #print("\n")
    resp = resp.replace(':false}',':"false"}')
    #print(resp)
    #print("\n")
    resp_json = json.loads(resp)
    #print(resp_json)

    #print("\n")
    print("-----------------------\n")
    print("hubspot API response: \n")
    print("\n")
    if "status" in resp_json:
      print("\n")
      print("status:")
      print(resp_json['status'])
      print("\n")
      if "message" in resp_json:
        print("\n")
        print("message: \n")
        print(resp_json['message'])
        print("\n")
        if (resp_json['message'] == "resource not found"):
          print("hubspot deal ID that you are trying to set does not exist! \n")
    print("\n")
    if "id" in resp_json: 
      print("\n")
      print("hubspot deal id:")
      print(resp_json['id'])
      print("\n")
      if "properties" in resp_json:
        print("\n")
        print("Success! Below is \"likelihood to close\" property according to your request:")
        print(resp_json['properties']['likelihood_to_close'])
        print("\n")

    print("-----------------------\n")

  except Exception as error :
      print ("Parsing json response from hubspot API: ", error)
      print("\n")
      print("full response from hubspot API: \n")
      print(resp)

  finally:
      print("\n")
      print("\n")
      print("==============================================\n")
      print("\n")

import datetime
      
print("Data Analytics \n")
now = datetime.datetime.now()
print ("Current date and time : ")
print (now.strftime("%Y-%m-%d %H:%M:%S"))

print("\n")

# CSV options
infer_schema = "false"
first_row_is_header = "false"
delimiter = ","

# The applied options are for CSV files. For other file types, these will be ignored.
df = spark.read.format(file_type) \
  .option("inferSchema", infer_schema) \
  .option("header", first_row_is_header) \
  .option("sep", delimiter) \
  .load(file_location)

display(df)

for row in df.rdd.collect():
    #print("current table record: \n")
    #print(row)
    print("\n")
    print("***********************************************\n")
    print("\n")
    print("hubspot deal ID that we are about to update: \n")
    print(row._c0)
    check_if_numeric_hubspot_deal_id = row._c0.isnumeric()
    print("\n")
    print("check if hubspot deal ID is numeric:")
    print(check_if_numeric_hubspot_deal_id)
    print("\n")
    if check_if_numeric_hubspot_deal_id:
      print("hubspot deal ID is numeric, thus we'll continue...")
      print("monolith deal ID: \n")
      print(row._c1)
      print("\n")
      print("deal_close_score value that we are about to send: \n")
      print(row._c2)
      check_if_numeric_score = row._c2.isnumeric()
      print("check if score is numeric:")
      print(check_if_numeric_score)
      print("\n")
      if check_if_numeric_score: 
        print("score is numeric, thus we'll continue...")
        print("\n")
        if check_if_numeric_score == True and (min_likelihood_to_close <= float(row._c2) <= max_likelihood_to_close): 
          print("score value is between possible min and max (min_likelihood_to_close & max_likelihood_to_close), thus we'll continue... \n")
          print("------------------begin hubspot operation")
          update_likelihood_to_close(row._c0, row._c2)
          print("------------------end hubspot operation")
        else: 
          print("score is not between possible min and max (min_likelihood_to_close & max_likelihood_to_close), thus we'll skip this one... \n")
        print("\n")
      else: 
        print("score is not numeric, thus we'll skip this one...")
    else: 
      print("row._c0 in csv (hubspot deal id) is not numeric, thus we'll skip this row...")