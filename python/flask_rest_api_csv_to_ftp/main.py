import sys
sys.path.append('/var/www/html/FlaskApp/FlaskApp/cgi-bin/lib/python3.6/site-packages')
from flask import Flask, render_template, request, Response, jsonify
import csv
#import snowflake.connector
import jwt
import time
from base64 import b64encode
import requests
#import os
#from os import walk
app = Flask(__name__)
#@app.route("/hello")
#def hello():
#if __name__ == "__main__":
#    app.run()

@app.route("/")
def home():
    return "The Guarantors. Please contact Data Analytics team if you have questions.\n"

def gcloud_api_to_csv(filename, request, delimiter, dir):
    if not filename:
#        print("Error: filename should not be empty!")
        raise Exception("Error: filename should not be empty!")
    if not request:
#        print("Error: sql request should not be empty!")
        raise Exception("Error: sql request should not be empty!")
    if not '.csv'.upper() in filename.upper():
#        print("Error: filename should contain .csv extention!")
        raise Exception("Error: filename should contain .csv extention!")
    if not 'select '.upper() in request.upper():
#        print("Error: sql request should contain SELECT!")
        raise Exception("Error: sql request should contain SELECT!")
    #var_apicreds = bytes(os.getenv('GCLOUDAPICREDS'), 'UTF-8')
    #print(var_apicreds)
    #time.sleep(55)
    ts = time.time()
    userAndPass = b64encode(b"user1:pass1").decode("ascii")
    var_headers = { 'Authorization' : 'Basic %s' %  userAndPass }
    var_filename = filename
    var_delimiter = delimiter
    url = 'google_cloud_api_endpoint_here/snowflake_to_csv?filename=' + var_filename + '&delimiter=' + str(var_delimiter)
    sql_request = request

    x = requests.post(url, data = sql_request, headers=var_headers)

    csv_path = "/var/run/vsftpd/empty/" + dir + "/reports/"

    if (x.text):
        with open(csv_path + var_filename, 'w') as f:
          print(x.text, file=f)
    else:
        with open(csv_path + var_filename, 'w') as f:
          print('Server response was empty.\nPlease send this information to Data Analytics team if you think this is an error.\nSQL request is below for your convenience:\n\ns' + sql_request + '\n\n\n' + ts, file=f)

def snowflake_csv(custom_query, filename, dir):
#custom_query = "SELECT 1234 As t5 Union All SELECT 2345 As t5"
#csv_path = "/var/www/html/FlaskApp/FlaskApp/csv/"
#    print("stage3a\n")
    csv_path = "/var/run/vsftpd/empty/" + dir + "/"

#myuser = os.getenv('SNOWFLAKEUSR')
#print("myuser: \n")
#print(myuser)
#print("\n")

# Gets the version
    print("stage3b\n")
    ctx = snowflake.connector.connect(
        user='user1',
        password='pass1',
        account='account1',
        warehouse="warehouse1",
        database="database1",
        schema="schema1"
        )
    cs = ctx.cursor()
    try:
#        print("stage3c\n")
        cs.execute(custom_query)
#        print("stage3d\n")
        rows=cs.fetchall()
#        print("stage3e\n")
        column_names = [i[0] for i in cs.description]
#        print("stage3f\n")
        fp = open(csv_path + filename, 'w')
#        print("stage3g\n")
        myFile = csv.writer(fp, lineterminator = '\n')
#        print("stage3h\n")
        myFile.writerow(column_names)
#        print("stage3i\n")
        myFile.writerows(rows)
#        print("stage3j\n")
        fp.close()
#        print("stage3k\n")
    #cs.fetch_pandas_all().to_csv(csv_path + "test.csv")
    #one_row = cs.fetchone()
    #print(one_row[0])
    finally:
#        print("stage3L\n")
        cs.close()
    ctx.close()
#    print("stage3z final\n")

def upload1(file):
    from pydrive.auth import GoogleAuth
    from pydrive.drive import GoogleDrive
    #from google.cloud import storage

    settings_path = 'settings.yaml'
    gauth = GoogleAuth(settings_file=settings_path)

    #gauth = GoogleAuth()

    gauth.LoadCredentialsFile("mycreds.txt")
    if gauth.credentials is None:
        gauth.LocalWebserverAuth()
    elif gauth.access_token_expired:
        gauth.Refresh()
    else:
        gauth.Authorize()
    gauth.SaveCredentialsFile("mycreds.txt")

    #storage_client = storage.Client()

    drive = GoogleDrive(gauth)
    #drive = GoogleDrive()
    #drive = storage.Client()

#    drives_list = drive.drives().list(pageSize=100).execute()
#    print("drives_list: \n")
#    print(drives_list['drives'][0]['id'])
#    print(drives_list['drives'][1]['id'])
#    print("\n")

#    file_list = ""
    team_drive_id = 'team_drive_id1'
    list_file_q = "{'q': \"'" + team_drive_id + "' in parents and trashed=False\", 'supportsAllDrives': True, 'driveId': '" + team_drive_id + "', 'includeItemsFromAllDrives': True, 'corpora': 'drive'}"
    print("list_file_q: \n")
    print(list_file_q)
    print("\n")
#    file_list = ""
    file_list = drive.ListFile(eval(list_file_q)).GetList()
    print("file_list:\n")
    print(file_list)
    print("\n")
    file_id = ""
    try:
        for file1 in file_list:
            print("file title: \n")
            print(file1['title'])
            print(file1['id'])
            print(file)
            print("\n")
            if file1['title'] == file:
                file_id = file1['id']
                print("file names do match, thus deleting... \n")
                file1.Delete()
                file1.Trash()
                drive.CreateFile({'id': file1['id']}).Delete()
            else:
                print("file names do NOT match, thus NOT deleting. \n")
    except:
        pass
 #   myfile = "\'" + file + "\'"
    #filespec = "{'title':" + myfile + ", 'mimeType':'text/csv', 'parents': [{'kind': 'drive#fileLink','id': '1NuOud0X4es-U6PN2erzSkiC4MLtE8QUW'}]}"
    #filespec = "{'mimeType':'text/csv', 'parents': [{'kind': 'drive#fileLink','id': '1NuOud0X4es-U6PN2erzSkiC4MLtE8QUW'}]}"
    #filespec = "{'parents': [{'id': '1NuOud0X4es-U6PN2erzSkiC4MLtE8QUW'}]}"
    #print("\n")
    #print("filespec: \n")
    #print(filespec)
    #print("\n")
#    file_id = ""
#    for x in range(len(file_list)):
#        if file_list[x]['title'] == file:
#           file_id = file_list[x]['id']
    #var_create_file = ""
    if not file_id:
        print("creating file from scratch... \n")
        f = drive.CreateFile({'parents': [{'kind': 'drive#fileLink', 'teamDriveId': team_drive_id, 'id': team_drive_id}]})
    else:
        print("updating file using existing file_id... \n")
        var_create_file = "{'id': " + file_id + ", 'parents': [{'kind': 'drive#fileLink', 'teamDriveId': " + team_drive_id + ", 'id': " + team_drive_id + "}]}"
        print("var_create_file: \n")
        print(var_create_file)
        print("\n")
        f = drive.CreateFile(eval(var_create_file))
        print("test after create file \n")
    print("before SetContentFile \n")
    f.SetContentFile(file)
    print("after SetContentFile \n")
    #f = drive.CreateFile(filespec)
    #f.SetContentFile("/tmp/" + file)
    print("before f.Upload \n")
    f.Upload(param={'supportsTeamDrives': True})
    print("after f.Upload \n")

@app.route("/snowflake_to_gdrive", methods=['POST'])
#@requires_auth 
def private_snowflake_to_gdrive():
#    print("stage1\n")
    filename = ""
    data = str(request.get_data())
    data = data[2:]
    data = data[:-1]
    if not data:
        return("Error: sql request is empty; please set non-empty sql request in request body (raw)!")

    if "current_timestamp as current_timestamp_x".upper() in data.upper():
        pass
    else:
        return("Error: sql request should contain 'current_timestamp as current_timestamp_x' so that tracking last updated time is possible!")

    if 'dir' in request.args: #key dir does not exist in url
        dir = request.args['dir']
    else:
        return("Error: no dir key present in request url!")
    if not dir: #key dir exists in url but contains no value
        return("Error: dir is empty; please set non-empty dir in request url!")

    if dir == 'data_finance' or dir == 'data_risk' or dir == 'data_underwriting' or dir == 'data_analytics':
        pass
    else:
        return("Error: dir possible values are: data_finance, data_risk, data_underwriting, data_analytics")

    if 'filename' in request.args: #key filename does not exist in url
        filename = request.args['filename']
    else:
        return("Error: no filename key present in request url!")
    if not filename: #key filename exists in url but contains no value
        return("Error: filename is empty; please set non-empty filename in request url!")

    if 'delimiter' in request.args: #key 'delimiter' does not exist in url
        delimiter = request.args['delimiter']
    else:
        return("Error: no delimiter key present in request url!")
    if not delimiter: #key 'delimiter' exists in url but contains no value
        return("Error: delimiter is empty; please set non-empty delimiter in request url!")
    check_delimiter = 0
    try:
        check_delimiter = int(delimiter)
    except Exception as error:
        return "Error when trying to parse delimiter number from url string (must be integer number only): " + str(error)
#    try:
#        if check_delimiter == 1:
#            pass
#        elif check_delimiter == 2:
#            pass
#        elif check_delimiter == 3:
#            pass
#        elif check_delimiter == 4:
#            pass
#        else:
#            raise DelimiterNotInRangeError(check_delimiter)
#    except Exception as error:
#        return "Delimiter number check failed: " + str(error)
    # check if filename contains '.csv'
    # check if sql request contains 'select '
    #print("\n")
    #print("data: \n")
    #print(data)
    #print("\n")
#    print("stage2\n")
#    try:
#        print("stage3\n")
#        snowflake_csv(data, filename)
#    except Exception as error:
#        print("Error while fetching data from Snowflake", error)
#        return "Snowflake error: " + str(error)
#    print("stage4\n")
    #print("\n")
    #print("file content: \n")
#    print("files in current dir : \n")
    #print(_, _, filenames = next(walk('/tmp')))
#    arr = next(os.walk('/var/www/html/FlaskApp/FlaskApp/tmp/'))[2]
#    print(arr)
#    print("\n")
#    with open('/var/www/html/FlaskApp/FlaskApp/tmp/' + filename) as f:
#        contents = f.read()
#        print(contents)
#        print("\n")
#    f.close()
    try:
        gcloud_api_to_csv(filename, data, check_delimiter, dir)
    except Exception as error:
        print("Error while fetching csv from Google Cloud API", error)
        return "Google Cloud API error: " + str(error)
#    try:
#        upload1('/x/' + filename)
#    except Exception as error:
#        print("Error while uploading data to Google Drive: ", error)
#        return "Google Drive error: " + str(error)
#    print("stage_final\n")
    return 'ok'

@app.errorhandler(404)
def not_found_error(error):
    return "404 not found!\n"
