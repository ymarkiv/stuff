from functools import wraps
from flask import Flask, request, Response, send_file
import snowflake.connector
import os
import csv
app = Flask(__name__)

class DelimiterNotInRangeError(Exception):
    """Exception raised for errors in the input delimiter.

    Attributes:
        delimiter -- input delimiter which caused the error
        message -- explanation of the error
    """

    def __init__(self, delimiter, message="Delimiter is not in (1, 4) range. Delimiter must be 1 for comma, 2 for semicolon, 3 for colon or 4 for tab. Please set one of possible values or ask Data Analytics team to add additional symbol."):
        self.delimiter = delimiter
        self.message = message
        super().__init__(self.message)

def check_auth(username, password):
    return username == 'user1' and password == 'pass1'

def authenticate():
    return Response(
    'Could not verify your access level for that URL.\n'
    'You have to login with proper credentials', 401,
    {'WWW-Authenticate': 'Basic realm="Login Required"'})

def requires_auth(f):
    @wraps(f)
    def decorated(*args, **kwargs):
        auth = request.authorization
        if not auth or not check_auth(auth.username, auth.password):
            return authenticate()
        return f(*args, **kwargs)
    return decorated

def snowflake_csv(custom_query, filename, delimiter):
#custom_query = "SELECT 1234 As t5 Union All SELECT 2345 As t5"
#csv_path = "/var/www/html/FlaskApp/FlaskApp/csv/"
    csv_path = "/tmp/"

#myuser = os.getenv('SNOWFLAKEUSR')
#print("myuser: \n")
#print(myuser)
#print("\n")

# Gets the version
    ctx = snowflake.connector.connect(
        user='user1',
        password='password1',
        account='account1',
        warehouse="warehouse1",
        database="database1",
        schema="schema1"
        )
    cs = ctx.cursor()
    try:
        cs.execute(custom_query)
        rows=cs.fetchall()
        column_names = [i[0] for i in cs.description]
        fp = open(csv_path + filename, 'w')
        #var_writer = "fp, delimiter=" + input_delimiter + ", lineterminator = '\n'"
        #print("\n")
        #print("var_writer:\n")
        #print(var_writer)
        #print("\n")
        if delimiter == 1:
            myFile = csv.writer(fp, delimiter=',', lineterminator = '\n')
        elif delimiter == 2:
            myFile = csv.writer(fp, delimiter=';', lineterminator = '\n')
        elif delimiter == 3:
            myFile = csv.writer(fp, delimiter=':', lineterminator = '\n')
        elif delimiter == 4:
            myFile = csv.writer(fp, delimiter='	', lineterminator = '\n')
        else:
            raise DelimiterNotInRangeError(delimiter)
        myFile.writerow(column_names)
        myFile.writerows(rows)
        fp.close()
    #cs.fetch_pandas_all().to_csv(csv_path + "test.csv")
    #one_row = cs.fetchone()
    #print(one_row[0])
    finally:
        cs.close()
    ctx.close()
    #try:
        #with open(csv_path + filename, "r") as file:
            #for item in file:
                #print(item)
                #content = item.split(',')
                #if content[0] == userName:
                #    return content[1]
            #return "-1"
    #except IOError:
        #print("File not found")
        #return "-1"

@app.route("/")
def hello():
    return "This is The Guarantors API -=snowflake to csv=-. Please contact Data Analytics team if you have questions."

@app.route("/snowflake_to_csv", methods=['POST'])
@requires_auth 
def private_snowflake_to_csv():
    filename = ""
    data = str(request.get_data())
    data = data[2:]
    data = data[:-1]
    if not data:
        return("Error: sql request is empty; please set non-empty sql request in request body (raw)!")
    if 'filename' in request.args: #key 'filename' does not exist in url
        filename = request.args['filename']
    else:
        return("Error: no filename key present in request url!")
    if not filename: #key 'filename' exists in url but contains no value
        return("Error: filename is empty; please set non-empty filename in request url!")
    if 'delimiter' in request.args: #key 'delimiter' does not exist in url
        delimiter = request.args['delimiter']
    else:
        return("Error: no delimiter key present in request url!")
    if not delimiter: #key 'delimiter' exists in url but contains no value
        return("Error: delimiter is empty; please set non-empty delimiter in request url!")
    check_delimiter = 0
    try:
        check_delimiter = int(delimiter)
    except Exception as error:
        return "Error when trying to parse delimiter number from url string (must be integer number only): " + str(error)
    try: 
        if check_delimiter == 1:
            pass
        elif check_delimiter == 2:
            pass
        elif check_delimiter == 3:
            pass
        elif check_delimiter == 4:
            pass
        else:
            raise DelimiterNotInRangeError(check_delimiter)
    except Exception as error: 
        return "Delimiter number check failed: " + str(error)
    # check if filename contains '.csv'
    # check if sql request contains 'select '
    #print("\n")
    #print("data: \n")
    #print(data)
    #print("\n")
    try:
        snowflake_csv(data, filename, int(delimiter))
    except Exception as error:
        print("Error while fetching data from Snowflake", error)
        return "Snowflake error: " + str(error)
    print("\n")
    print("file content: \n")
    with open('/tmp/' + filename) as f:
        contents = f.read()
        print(contents)
        print("\n")
    f.close()
    #try:
    #    upload1('/tmp/' + filename)
    #except Exception as error:
    #    print("Error while uploading data to Google Drive", error)
    #    return "Google Drive error: " + str(error)
    return send_file(os.path.join('/tmp/', filename))
    #return 'ok'
    #return data
    #return bytes("/* " + filename + " */ \n", 'utf-8') + data

if __name__ == "__main__":
    app.run(debug=True)
