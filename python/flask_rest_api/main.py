from functools import wraps
from flask import Flask, request, Response
app = Flask(__name__)

def check_auth(username, password):
    return username == 'admin' and password == 'secret'

def authenticate():
    return Response(
    'Could not verify your access level for that URL.\n'
    'You have to login with proper credentials', 401,
    {'WWW-Authenticate': 'Basic realm="Login Required"'})

def requires_auth(f):
    @wraps(f)
    def decorated(*args, **kwargs):
        auth = request.authorization
        if not auth or not check_auth(auth.username, auth.password):
            return authenticate()
        return f(*args, **kwargs)
    return decorated

@app.route("/")
def hello():
    return "_Hello___World!_"

@app.route("/private")
@requires_auth 
def private_page():
    return "Hello I'm Private!!"

@app.route("/private2")
@requires_auth 
def private_page2():
    return "Hello I'm Private 2!!"

@app.route("/private3")
@requires_auth 
def private_page3():
    arg1 = request.args['arg1']
    arg2 = request.args['arg2']
    return "Hello I'm Private 3!! " + arg1 + " " + arg2

if __name__ == "__main__":
    app.run()
