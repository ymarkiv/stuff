#!/usr/bin/env python
import psycopg2
from psycopg2.extensions import AsIs
import snowflake.connector
import os
#from psycopg2 import sql

isDelete = True

print("\n")

import datetime
now = datetime.datetime.now()
print ("Current date and time : ")
print (now.strftime("%Y-%m-%d %H:%M:%S"))

print("\n")

# settings (double quotes must be escaped) : [["source_table1","target_table1","primary_key1_source","primary_key1_target"],["source_table2","target_table2","primary_key2_source","primary_key2_target"],["\"source_Table3\"","\"target_Table3\"","\"primary_Key3_source\"","\"primary_Key3_target\""]]
tables = [["sandbox.\"test1\"","TEST_SANDBOX.DW_SANDBOX.SANDBOX_TEST1","id","ID"]]

for x in tables:
  print("=============================================================\n")
  print("\n")
  print("\n")
  print("table settings:\n")
  print(x)
  print("\n")
  print("source table:\n")
  print(x[0])
  print("\n")
  print("target table:\n")
  print(x[1])
  print("\n")
  print("primary key column:\n")
  print(x[2])
  print("\n")
  print("\n")
  print("\n")
  print("\n")

  var_source = x[0]
  var_target = x[1]
  var_column_source = x[2]
  var_column_target = x[3]

  postgres_array = []
  snowflake_array = []

  print("Postgres start \n")

  try:
     postgres_connection = psycopg2.connect(user="dw_user",
                                    host="172.22.2.177",
                                    port="5432",
                                    database="theguarantors_staging")
     cursor = postgres_connection.cursor()
#     postgreSQL_select_Query = "Select id From sandbox.test_deleted"

     cursor.execute("Select %(column)s From %(table)s;", {"table": AsIs(var_source), "column": AsIs(var_column_source)})
#       sql.SQL("Select id From {}")
#         .format(sql.Identifier(x[0]))
#       )
     print("Selecting rows from Postgres Monolith staging table test_deleted using cursor.fetchall")
     test_deleted_records = cursor.fetchall() 
     
     print("Print each row and its columns values")
     for row in test_deleted_records:
         print(row[0])
         postgres_array.append(row[0])





     print("\n")
     print("\n")
     print("\n")

     print("Snowflake start \n")

     try:
        snowflake_connection = snowflake.connector.connect(
            user=os.getenv('SNOWFLAKEUSR'),
            password=os.getenv('SNOWFLAKEPWD'),
            account=os.getenv('SNOWFLAKEACC'),
            role="SYSADMIN",
            warehouse="TG_USER_WH",
            database="TEST_SANDBOX",
            schema="DW_SANDBOX"
            )

#        snowflake_connection = psycopg2.connect(user="theguarantors",
#                                       host="theguarantors.periscopewarehouse.com",
#                                       port="5439",
#                                       database="site_14899")

        cur = snowflake_connection.cursor()

#        cur.execute("Select %(column)s From %(table)s;", {"table": AsIs(var_target), "column": AsIs(var_column_target)})
        cur.execute("Select " + x[3] + " From " + x[1])
        for row in cur:
            print(row[0])
            snowflake_array.append(row[0])

        print("\n")
        print("\n")
        print("\n")

        print("postgres array: \n")
        print(postgres_array)
        print("\n")
        print("\n")
        print("\n")

        print("snowflake array: \n")
        print(snowflake_array)
        print("\n")
        print("\n")

        print("compare arrays postgres vs snowflake: \n")
#	var_to_delete = ""
#	k = 0
#        for i in snowflake_array:
#           if ((i not in postgres_array) and (len(postgres_array)>0)):
#	      print("\n")
#	      print("k=\n")
#	      print(k)
#	      print("\n")
#	      print("\n")
#             print(i)
#             print("delete ID from Snowflake table:" + str(i) + "\n")
     #        s = ""
     #        s += "Update sandbox.test_deleted Set is_deleted = True"
     #        s += " Where "
     #        s += "("
     #        s += " id = (%i)"
     #        s += ")"
     #        print(s)
     #        snowflake_query = "Update sandbox.test_deleted Set is_deleted = True Where id = 12"
#             cur.execute("Delete From %(table)s Where %(column)s in ( %(value)s );", {"table": AsIs(var_target), "column": AsIs(var_column), "value": AsIs(i)})
#	      if (k == 0):
#	         var_to_delete = var_to_delete + str(i)
#	      else: 
#	         var_to_delete = var_to_delete + "," + str(i)
#	      k = k + 1

        print("\n")
#	print("values to delete: ")
#	print(var_to_delete)
        print("\n")

        delta_array = list(set(snowflake_array) - set(postgres_array))
        delta_comma = ','.join([str(x) for x in delta_array])
        print("\n")
        print("delta array: ")
        print(delta_array)
        print("\n")
        print("comma separated delta list: ")
        print(delta_comma)
        print("\n")
        print("\n")
        print("isDelete value:\n")
        print(isDelete)
        print("\n")
        if (len(delta_array)>0 and isDelete):
            print("Deleting delta from target table\n")
            cur.execute("Delete From " + x[1] + " Where " + x[3] + " In (" + delta_comma + ");")
        print("\n")
#	if (len(delta_array)>0):
#	    cur.execute("Delete From %(table)s Where %(column)s in ( %(value)s );", {"table": AsIs(var_target), "column": AsIs(var_column), "value": AsIs(delta_comma)})
#       cursor.execute("Insert Into periscope_views.log_deleted_ids (source_table, target_table, key_column, source_ids, target_ids, delta_ids, updated_at) Select ' %(source_table_value)s ' As source_table, ' %(target_table_value)s ' As target_table, ' %(column_value)s ' As key_column, ' %(postgres_array_value)s ' As source_ids, ' %(snowflake_array_value)s ' As target_ids, ' %(delta_array_value)s ' As delta_ids, Now() as updated_at;", {"source_table_value": AsIs(var_source), "target_table_value": AsIs(var_target), "column_value": AsIs(var_column), "postgres_array_value": AsIs(postgres_array), "snowflake_array_value": AsIs(snowflake_array), "delta_array_value": AsIs(delta_array)})
        print("\n")
        print("\n")
        print("\n")
     except (Exception, psycopg2.Error) as error :
         print ("Error while fetching data from Snowlake", error)

     finally:
         #closing database connection.
         if(snowflake_connection):
#             snowflake_connection.commit()
             cur.close()
             snowflake_connection.close()
             print("Snowflake connection is closed")

     print("\n")
     print("Snowflake end \n")
     print("\n")





  except (Exception, psycopg2.Error) as error :
      print ("Error while fetching data from PostgreSQL", error)

  try:
     target_comma = ','.join([str(x) for x in snowflake_array])
#     print(target_comma)
     target_comma_to_array = [int(x) for x in target_comma.split(',') if x.strip().isdigit()]
     delta_array_backwards = list(set(postgres_array) - set(target_comma_to_array))
     print("\n")
     print("delta_array_backwards: \n")
     print(delta_array_backwards)
     print("\n")
#     if (len(delta_array)>0):
#         cursor.execute("Insert Into periscope_views.log_deleted_ids (source_table, target_table, key_column, source_ids, target_ids, delta_ids, updated_at) Select ' %(source_table_value)s ' As source_table, ' %(target_table_value)s ' As target_table, ' %(column_value)s ' As key_column, ' %(postgres_array_value)s ' As source_ids, ' %(snowflake_array_value)s ' As target_ids, ' %(delta_array_value)s ' As delta_ids, Now() as updated_at;", {"source_table_value": AsIs(var_source), "target_table_value": AsIs(var_target), "column_value": AsIs(var_column), "postgres_array_value": AsIs(postgres_array), "snowflake_array_value": AsIs(target_comma_to_array), "delta_array_value": AsIs(delta_array)})
  except (Exception, psycopg2.Error) as error :
     print ("Error while trying to store log", error)

  finally:
      #closing database connection.
      if(postgres_connection):
          postgres_connection.commit()
          cursor.close()
          postgres_connection.close()
          print("PostgreSQL connection is closed")

  print("Postgres end \n")

