public class Learn
{
  public static String reverseWords(final String original)
  /*accept a string parameter and reverse each word in the string (all spaces in the string should be retained)*/
  {
    String result = "";
    if (original.length() == original.length() - original.replaceAll(" ", "").length()){
      result = original;
    }
    String[] words = original.split(" ");
    for (int i=0;i<words.length;i++){
      String reversed = "";
      for (int j=words[i].length()-1;j>=0;j--){
        reversed = reversed + words[i].substring(j,j+1);
      }
      String space = " ";
      if (i==(words.length-1)){
        space = "";
      }
      result = result + reversed + space;
    }
    return result;
  }
} 
