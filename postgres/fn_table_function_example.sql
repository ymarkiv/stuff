Create Or Replace Function periscope_views.fn_calc_total (
    "in_Category" Varchar(100), 
    "in_Date" date, 
    "in_Amount" decimal
) 
	Returns Table (
		"Category" Varchar(100),
		"Date" date,
		"Input Amount" decimal,
		"Tier1" decimal,
		"Tier1 percent" decimal,
		"Tier2" decimal,
		"Tier2 percent" decimal,
		"Tier3" decimal,
		"Tier3 percent" decimal,
		"Tier4" decimal,
		"Tier4 percent" decimal,
		"Total" decimal
	) 
	Language plpgsql
As $$
Declare 
    var_doc Varchar(1000) = 'https://docs.google.com/document/d/1234567890/edit#';
    var_exception Varchar(1000);
Begin
	If (Select Count(1) From periscope_views."settingsMSA" t Where t."Category" = "in_Category")=0 Then 
	    Raise Exception '% Please set in_Category input parameter value according to existing values in periscope_views.settingsMSA table (Category column); % % see more information in % %', chr(10), chr(10), chr(10), var_doc, chr(10);
	End If;
	If (Select Count(1) From periscope_views."settingsMSA" t Where t."Category" = "in_Category" And t."Name" = 'tier1' And t."Start Date" <= "in_Date" And "in_Date" <= t."End Date")=0 Then 
	    Raise Exception '% Please add actual tier1 parameter for % category, % input date into settings table periscope_views.settingsMSA table; % % see more information in % %', chr(10), "in_Category", "in_Date", chr(10), chr(10), var_doc, chr(10);
	End If;
	If (Select Count(1) From periscope_views."settingsMSA" t Where t."Category" = "in_Category" And t."Name" = 'tier2' And t."Start Date" <= "in_Date" And "in_Date" <= t."End Date")=0 Then 
	    Raise Exception '% Please add actual tier2 parameter for % category, % input date into settings table periscope_views.settingsMSA table; % % see more information in % %', chr(10), "in_Category", "in_Date", chr(10), chr(10), var_doc, chr(10);
	End If;
	If (Select Count(1) From periscope_views."settingsMSA" t Where t."Category" = "in_Category" And t."Name" = 'tier3' And t."Start Date" <= "in_Date" And "in_Date" <= t."End Date")=0 Then 
	    Raise Exception '% Please add actual tier3 parameter for % category, % input date into settings table periscope_views.settingsMSA table; % % see more information in % %', chr(10), "in_Category", "in_Date", chr(10), chr(10), var_doc, chr(10);
	End If;
	If (Select Count(1) From periscope_views."settingsMSA" t Where t."Category" = "in_Category" And t."Name" = 'tier4' And t."Start Date" <= "in_Date" And "in_Date" <= t."End Date")=0 Then 
	    Raise Exception '% Please add actual tier4 parameter for % category, % input date into settings table periscope_views.settingsMSA table; % % see more information in % %', chr(10), "in_Category", "in_Date", chr(10), chr(10), var_doc, chr(10);
	End If;
	If (Select Count(1) From periscope_views."settingsMSA" t Where t."Category" = "in_Category" And t."Name" = 'tier1' And t."Start Date" <= "in_Date" And "in_Date" <= t."End Date")>1 Then 
	    Raise Exception '% More than one actual tier1 parameter exists for % category, % input date in settings table periscope_views.settingsMSA table; % % please fix settings so that only one actual tier1 parameter is present for that category! % % see more information in % %', chr(10), "in_Category", "in_Date", chr(10), chr(10), chr(10), chr(10), var_doc, chr(10);
	End If;
	If (Select Count(1) From periscope_views."settingsMSA" t Where t."Category" = "in_Category" And t."Name" = 'tier2' And t."Start Date" <= "in_Date" And "in_Date" <= t."End Date")>1 Then 
	    Raise Exception '% More than one actual tier2 parameter exists for % category, % input date in settings table periscope_views.settingsMSA table; % % please fix settings so that only one actual tier2 parameter is present for that category! % % see more information in % %', chr(10), "in_Category", "in_Date", chr(10), chr(10), chr(10), chr(10), var_doc, chr(10);
	End If;
	If (Select Count(1) From periscope_views."settingsMSA" t Where t."Category" = "in_Category" And t."Name" = 'tier3' And t."Start Date" <= "in_Date" And "in_Date" <= t."End Date")>1 Then 
	    Raise Exception '% More than one actual tier3 parameter exists for % category, % input date in settings table periscope_views.settingsMSA table; % % please fix settings so that only one actual tier3 parameter is present for that category! % % see more information in % %', chr(10), "in_Category", "in_Date", chr(10), chr(10), chr(10), chr(10), var_doc, chr(10);
	End If;
	If (Select Count(1) From periscope_views."settingsMSA" t Where t."Category" = "in_Category" And t."Name" = 'tier4' And t."Start Date" <= "in_Date" And "in_Date" <= t."End Date")>1 Then 
	    Raise Exception '% More than one actual tier4 parameter exists for % category, % input date in settings table periscope_views.settingsMSA table; % % please fix settings so that only one actual tier4 parameter is present for that category! % % see more information in % %', chr(10), "in_Category", "in_Date", chr(10), chr(10), chr(10), chr(10), var_doc, chr(10);
	End If;
	If (Select count(1) From periscope_views."settingsMSA" t Where t."Category" = "in_Category" And t."Name" like 'tier%' And t."Start Date" <= "in_Date" And "in_Date" <= t."End Date" And t."Limit" < 0)>0 Then 
	    Raise Exception '% One of tier limit parameter is negative, % category in settings table periscope_views.settingsMSA table; % % please fix settings so that only non-negative tier1 parameter values are present! % % see more information in % %', chr(10), "in_Category", chr(10), chr(10), chr(10), chr(10), var_doc, chr(10);
	End If;
	If (Select Count(1) From periscope_views."settingsMSA" t Where t."Category" = "in_Category" And t."Name" = 'discount rate' And t."Start Date" <= "in_Date" And "in_Date" <= t."End Date")=0 Then 
	    Raise Exception '% Please add actual "discount rate" parameter for % category, % input date into settings table periscope_views.settingsMSA table; % % see more information in % %', chr(10), "in_Category", "in_Date", chr(10), chr(10), var_doc, chr(10);
	End If;
	If (Select Count(1) From periscope_views."settingsMSA" t Where t."Category" = "in_Category" And t."Name" = 'Price per unit' And t."Start Date" <= "in_Date" And "in_Date" <= t."End Date")=0 Then 
	    Raise Exception '% Please add actual "Price per unit" parameter for % category, % input date into settings table periscope_views.settingsMSA table; % % see more information in % %', chr(10), "in_Category", "in_Date", chr(10), chr(10), var_doc, chr(10);
	End If;
	Return Query 
		With tsql As (
		    Select t."Name", t."Limit", t."Rate"
		    From periscope_views."settingsMSA" t 
		    Where t."Start Date" <= "in_Date" 
		      And "in_Date" <= t."End Date"
		), 
		tiers As (
		Select 
    		"in_Category" As "Category",
    		"in_Date" As "Date",
    		Case When "in_Amount" <= (Select u."Limit" From tsql u Where u."Name" = 'tier1') 
    		    Then "in_Amount" 
    		    Else (Select u."Limit" From tsql u Where u."Name" = 'tier1') 
    		End As "Tier1",
    		Case When "in_Amount" <= (Select u."Limit" + 0.01 From tsql u Where u."Name" = 'tier1') 
    		    Then 0.00 
    		    Else 
    		        Case When "in_Amount" <= (Select u."Limit" From tsql u Where u."Name" = 'tier2')
    		            Then "in_Amount" - (Select u."Limit" From tsql u Where u."Name" = 'tier1')
    		            Else (Select u."Limit" From tsql u Where u."Name" = 'tier2') - (Select u."Limit" From tsql u Where u."Name" = 'tier1')
    		        End
    		End As "Tier2",
    		Case When "in_Amount" <= (Select u."Limit" + 0.01 From tsql u Where u."Name" = 'tier2') 
    		    Then 0.00 
    		    Else 
    		        Case When "in_Amount" <= (Select u."Limit" From tsql u Where u."Name" = 'tier3')
    		            Then "in_Amount" - (Select u."Limit" From tsql u Where u."Name" = 'tier2')
    		            Else (Select u."Limit" From tsql u Where u."Name" = 'tier3') - (Select u."Limit" From tsql u Where u."Name" = 'tier2')
    		        End
    		End As "Tier3",
    		Case When "in_Amount" <= (Select u."Limit" + 0.01 From tsql u Where u."Name" = 'tier3') 
    		    Then 0.00 
    		    Else "in_Amount" - (Select u."Limit" From tsql u Where u."Name" = 'tier3')
    		End As "Tier4"
    	), 
    	percentage As (
    	Select 
    	    t."Category", 
    	    t."Date", 
    	    "in_Amount" * 1.00 As "Input Amount",
    	    t."Tier1",
    	    t."Tier1" * (Select u."Rate" From tsql u Where u."Name" = 'tier1') As "Tier1 percent",
    	    t."Tier2",
    	    t."Tier2" * (Select u."Rate" From tsql u Where u."Name" = 'tier2') As "Tier2 percent",
    	    t."Tier3",
    	    t."Tier3" * (Select u."Rate" From tsql u Where u."Name" = 'tier3') As "Tier3 percent",
    	    t."Tier4",
    	    t."Tier4" * (Select u."Rate" From tsql u Where u."Name" = 'tier4') As "Tier4 percent"
    	From tiers t
    	)
    	Select 
    	    t."Category", 
    	    t."Date", 
    	    t."Input Amount",
    	    t."Tier1" * 1.00 As "Tier1",
    	    t."Tier1 percent",
    	    t."Tier2" * 1.00 As "Tier2",
    	    t."Tier2 percent",
    	    t."Tier3" * 1.00 As "Tier3",
    	    t."Tier3 percent",
    	    t."Tier4" * 1.00 As "Tier4",
    	    t."Tier4 percent", 
    	    t."Tier1 percent" + t."Tier2 percent" + t."Tier3 percent" + t."Tier4 percent" As "Total"
    	    From percentage t;
End;$$