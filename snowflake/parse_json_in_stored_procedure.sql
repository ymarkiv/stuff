CREATE OR REPLACE PROCEDURE parse_json1()
RETURNS String
LANGUAGE JAVASCRIPT
AS
$$

var query1a = `Drop TABLE If Exists tmp_json_questionary_answers_parsed;`
var query1b = `Create TABLE tmp_json_questionary_answers_parsed (VALUE1 VARCHAR(2000), VALUE2 VARCHAR(2000));`

var statement1a = snowflake.createStatement({sqlText: query1a});
var statement1b = snowflake.createStatement({sqlText: query1b});

statement1a.execute();
statement1b.execute();

var query1 = `Drop TABLE If Exists tmp_json_questionary_answers;`
var query2 = `Create TEMPORARY TABLE tmp_json_questionary_answers (VALUE VARCHAR(2000));`

var query3 = `Create Or Replace sequence json_questionary_answers_seq_01 start = 1 increment = 1;`

var query4 = `Insert Into tmp_json_questionary_answers (VALUE)
With data As (
  Select parse_json('{
  "replyId": 77,
  "questionnaireId": 1,
  "referenceType": "invitation",
  "referenceId": 4,
  "pageId": 4,
  "completed": false,
  "answers": [
    {
      "value": "Laura",
      "fieldId": 1
    },
    {
      "value": "Sstoragetest",
      "fieldId": 2
    },
    {
      "value": "1990-02-20",
      "fieldId": 3
    },
    {
      "value": "+1 122322322",
      "fieldId": 4
    },
    {
      "value": "skip",
      "fieldId": 5
    },
    {
      "value": "true",
      "fieldId": 7
    },
    {
      "value": "no",
      "fieldId": 8
    },
    {
      "value": "retired",
      "fieldId": 9
    },
    {
      "value": "34543",
      "fieldId": 10
    },
    {
      "fieldId": 11,
      "value": "435115577"
    },
    {
      "value": "false",
      "fieldId": 12
    },
    {
      "value": "true",
      "fieldId": 13
    }
  ]
}') As json
),
myvalue As (
    Select VALUE From Table (flatten(input => Select json From data)) f Limit 1
),
answers As (
    Select VALUE From Table (flatten(input => Select VALUE from myvalue))
)
Select VALUE From answers;`

var query5 = `Drop TABLE If Exists tmp_json_questionary_answers_value;`
var query6 = `Create Temporary Table tmp_json_questionary_answers_value (id int, VALUE varchar(1000));`

var query7 = `Insert Into tmp_json_questionary_answers_value (id, VALUE)
Select json_questionary_answers_seq_01.nextval As id, VALUE From tmp_json_questionary_answers;`

var query8 = `Select Max (id) As MAXID From tmp_json_questionary_answers_value;`

var statement1 = snowflake.createStatement({sqlText: query1});
var statement2 = snowflake.createStatement({sqlText: query2});
var statement3 = snowflake.createStatement({sqlText: query3});
var statement4 = snowflake.createStatement({sqlText: query4});
var statement5 = snowflake.createStatement({sqlText: query5});
var statement6 = snowflake.createStatement({sqlText: query6});
var statement7 = snowflake.createStatement({sqlText: query7});
var statement8 = snowflake.createStatement({sqlText: query8});

var _resultSet1 = statement1.execute();
var _resultSet2 = statement2.execute();
var _resultSet3 = statement3.execute();
var _resultSet4 = statement4.execute();
var _resultSet5 = statement5.execute();
var _resultSet6 = statement6.execute();
var _resultSet7 = statement7.execute();
var _resultSet8 = statement8.execute();

_resultSet8.next();
var maxid = _resultSet8.getColumnValue(1);

var i = 1;

while (i <= maxid) {

  var query9 = `With data As (
      Select parse_json(VALUE) As VALUE From tmp_json_questionary_answers_value Where id = i
  )
  Select * From Table (flatten (input => Select VALUE From data));`

  /*var statement9 = snowflake.createStatement({sqlText: query9});*/
  var statement9 = snowflake.createStatement({sqlText: `With data As (
      Select parse_json(VALUE) As VALUE From tmp_json_questionary_answers_value Where id = :1
  )
  Select * From Table (flatten (input => Select VALUE From data));`, binds: [i]});

  var _resultSet9 = statement9.execute();

  _resultSet9.next();
  var val1 = _resultSet9.getColumnValue(5);
  _resultSet9.next();
  var val2 = _resultSet9.getColumnValue(5);

  var statement12 = snowflake.createStatement({sqlText: `Insert Into tmp_json_questionary_answers_parsed (VALUE1, VALUE2) Select :1 As VALUE1, :2 As VALUE2;`, binds: [val1, val2]});
  statement12.execute();

  i += 1;
}
return 'success';
$$;

Call parse_json1();

SELECT * FROM tmp_json_questionary_answers_parsed;