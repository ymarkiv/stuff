function is_int(value){
   for (i = 0 ; i < value.length ; i++) {
      if ((value.charAt(i) < '0') || (value.charAt(i) > '9')) return false 
    }
   return true;
}

function custom_concat(arg1, arg2) {
  return arg1 + ' and ' + arg2;
}

function custom_concat2(arg1, arg2) {
  return arg1 + ' or ' + arg2;
}

function custom_concat2(arg1, arg2) {
  return arg1 + ' or ' + arg2;
}

function custom_quality_deal_id(arg1) {
  if (isNaN(arg1)){
    return 0;
  } else {
    if (is_int(arg1)) {
      return 1;
    } else {
      return 0;
    }
  }
}

function custom_quality_score(arg2) {
  if (isNaN(arg2)){
    return 0;
  } else {
    if (1 <= arg2 && arg2 <= 100 ) {
      if (is_int(arg2)) {
        return 1;
      } else {
        return 0;
      }
    } else {
      return 0;
    }
  }
}

function call_api_wo_auth() {
  var url = 'https://example.com';
  var response = UrlFetchApp.fetch(url);
  var json = response.getContentText();
  return json;
}

function call_api_with_auth(arg1a, arg2a) {
  if (custom_quality_deal_id(arg1a) == 1 && custom_quality_score(arg2a) == 1) {
    var USERNAME = 'admin';
    var PASSWORD = 'secret';

    var headers = {
      "Authorization" : "Basic " + Utilities.base64Encode(USERNAME + ':' + PASSWORD)
    };

    var params = {
      "method":"GET",
      "headers":headers
    };

    var url = 'https://example.com/private3?arg1=' + arg1a + '&arg2=' + arg2a;
    var response = UrlFetchApp.fetch(url, params);
    var json = response.getContentText();
    return json;
  } else {
    return 'input parameters failed quality control check';
  }
}